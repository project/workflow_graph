********************************************************************
D R U P A L M O D U L E
********************************************************************
Name:
Workflow Graph Module
Author: Karim Ratib <karim dot ratib at open dash craft dot com>
Drupal: 5
********************************************************************
DESCRIPTION:

This module generates graphs from workflow definitions and instances.
It currently generates two distinct kinds of graphs:
* One for the workflow definition, showing all workflow states, the
  transitions linking them, and the roles allowed to perform those
  transitions.
* One for a workflow *instance history*, i.e. the path that a workflow
  took for a specific node. Here, the graph shows all state transitions 
  that occurred on the node, including the timestamp and the users who
  performed those transitions.

The module depends on Graphviz (http://www.graphviz.org) and
PEAR::Image_GraphViz (http://pear.php.net/package/Image_GraphViz).

********************************************************************
